# Introduction

This repository is a tutorial for auto-deploying a Spring Boot app with CI/CD on a VPS. 

Before using the project, rename everything where you find 'rename.me'.

Delete the files named remove-me.

Don't forget to update application.properties

# Deploy to VPS

## First: Create GitLab Token

1. Go to your GitLab repository:
    - Settings > Repository > Deploy tokens > Add token

2. Name it 'gitlab-deploy-token' and set permissions to read_registry.

This will create 3 environment variables (CI_REGISTRY, CI_DEPLOY_USER, CI_DEPLOY_PASSWORD) which are needed to use GitLab Docker repository. You can use Docker Hub if you want, but this version is the most useful.

## Second: Create SSH Key

1. Connect to your VPS and use these commands:
    - ssh-keygen (press enter without parameters)
    - ssh-copy-id username@remote_host (e.g., ubuntu@127.0.0.1)
    - cd .ssh
    - cat id_rsa

2. Copy the key and paste it into a file on your computer.

3. Next, open PuttyGen:
    - File > Load private key (select the file)
    - Conversions > Export OpenSSH Key

## Third: Create GitLab Environment Variables

1. Go to your GitLab repository:
    - Settings > CI/CD > Variables > Add variable

2. Set the following variables:
    - MASTER_HOST: remote_host as Variable
    - MASTER_SSH_USER: username as Variable
    - MASTER_SSH_KEY: SSH key (generated with PuttyGen) as File
    - MASTER_SSH_KEY: SSH key (generated with PuttyGen) as File
    - DB_URL
    - DB_USERNAME
    - DB_PASSWORD
    - PORT

You can set them as protected if you want.
